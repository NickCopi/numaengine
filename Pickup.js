class Pickup extends RectangleEntity {
    constructor(x, y, width, height) {
        super(x, y, width, height);
    }
    render(ctx, adjusted) {
        ctx.fillRect(adjusted.x, adjusted.y, this.width, this.height);
    }
    use(player) {
        player.hp += 10;
    }
}