class Weapon {
	constructor(weaponData) {
		this.speed = weaponData.speed;
		this.spread = weaponData.spread;
		this.spray = weaponData.spray
		this.flechettes = weaponData.flechettes;
		this.pierce = weaponData.pierce;
		this.fireDelay = weaponData.fireDelay;
		this.damage = weaponData.damage;
		this.radius = weaponData.radius;
		this.timeout = 0;
		this.capacity = weaponData.capacity;
		this.reloadBase = weaponData.reload;
		this.range = weaponData.range;
		this.auto = weaponData.auto;
		this.sprite = weaponData.sprite;
		this.mag = this.capacity;
		this.reloading = false;
	}
	shoot(player, bullets, time) {
		if (this.timeout > time || this.reloading)
			return;
		for (let i = -this.spread / 2; i < this.spread / 2; i += this.spread / this.flechettes) {
			let bullet = new Bullet(player.getCenter().x, player.getCenter().y, this.speed, this.damage, this.pierce, this.radius, this.range, this.sprite);
			let randomSpray = Math.random() * this.spray * 2 - this.spray;
			bullet.setDirection(player.angle + i + randomSpray);
			bullets.push(bullet);
		}
		this.mag--;
		if (this.mag === 0) {
			this.reloadTime = time + this.reloadBase;
			this.reloading = true;
		}
		this.timeout = time + this.fireDelay;
	}
	reload(time) {
		if (this.reloadTime <= time) {
			this.mag = this.capacity;
			this.reloading = false;
		}
	}
}