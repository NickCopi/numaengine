class RectangleEntity {
    constructor(x, y, width, height, angle = 0) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.angle = angle;
        this.calcLineSegments();
    }
    getCenterX() {
        return this.x + this.width / 2;
    }
    getCenterY() {
        return this.y + this.height / 2;
    }
    render(ctx, adjusted) {
        ctx.save();
		ctx.translate(adjusted.x + (this.width/2), adjusted.y + (this.height/2));
		ctx.rotate(this.angle);
		ctx.fillRect(this.width / -2, this.height / -2, this.width, this.height);
		ctx.restore();
    }
    calcLineSegments(overrideAngle) {
        const angle = overrideAngle === undefined ? this.angle : overrideAngle;
        this.lineSegments = [
            new LineSegment(
                new Point(this.x, this.y),
                new Point(this.x + this.width, this.y)
            ),
            new LineSegment(
                new Point(this.x + this.width, this.y),
                new Point(this.x + this.width, this.y + this.height)
            ),
            new LineSegment(
                new Point(this.x, this.y + this.height),
                new Point(this.x + this.width, this.y + this.height)
            ),
            new LineSegment(
                new Point(this.x, this.y),
                new Point(this.x, this.y + this.height)
            )
        ]

        this.lineSegments.forEach(segment => {
            segment.rotateAroundPoint(this.getCenterX(), this.getCenterY(), angle);
        });
    }
    /* This can be overriden on children that want to rebuild their line segments */
    getLineSegments(){
        return this.lineSegments;
    }

    collides(entity){
        return this !== entity && this.getLineSegments().some(segment=>{
            return entity.getLineSegments().some(entitySegment=>{
                return segment.intersects(entitySegment);
            });
        });
    }
}