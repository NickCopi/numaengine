class Interactable extends RectangleEntity {
    constructor(x, y, width, height) {
        super(x, y, width, height);
        this.interactCooldown = 60;
        this.lastInteract = -this.interactCooldown;
    }
    interact(time) {
        if (this.lastInteract + this.interactCooldown > time) return;
        console.log("Interactable.interact() not implemented");
        this.lastInteract = time;
    }
    render(ctx, adjusted) {
        ctx.fillRect(adjusted.x, adjusted.y, this.width, this.height);
    }
}