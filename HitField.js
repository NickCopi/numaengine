class HitField extends RectangleEntity {
	constructor(x, y, width, height, dmg) {
		super(x, y, width, height);
		this.active = true;
		this.dmg = dmg;
	}
	hit(target) {
		if (!this.active) return;
		if (this.collides(target)) {
			this.active = false;
			target.damage(this.dmg);
		}
	}
}