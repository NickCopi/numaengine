let game;
let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');
let spriteManager = new SpriteManager();


class Game {
	constructor() {
		this.sprites = spriteManager.sprites;
		this.settings = new Settings();
		this.scene = new Scene(500, 400, 3000, 3000, this.settings, this.sprites);
	}

}

class Settings {
	constructor() {
		this.UP_KEY = 87;
		this.DOWN_KEY = 83;
		this.LEFT_KEY = 65;
		this.RIGHT_KEY = 68;
		this.INTERACT_KEY = 32;
		this.RELOAD_KEY = 82;
		this.AI_DEBUG = true;
		this.HIT_DEBUG = true;
		this.MINIMAP_ENABLED = false;
		this.SEGMENT_DEBUG = true;
	}
}

class Scene {
	constructor(playerX, playerY, width, height, settings, sprites) {
		this.settings = settings;
		this.sprites = sprites;
		this.width = width;
		this.height = height;
		this.bullets = [];
		this.obstacles = [];
		this.pickups = [];
		this.interactables = [];
		this.enemies = [];
		this.faders = [];
		this.hitFields = [];
		this.spawners = [];
		this.keys = [];
		this.time = 0;

		this.gridSize = 75;

		this.astarGrid = new AstarGrid(this.gridSize);
		this.miniMap = new MiniMap(this.width, this.height);
		this.initMap();
		this.initControls();
		/*this.tileCutter = new TileCutter(this.width,this.height,this.obstacles);
		this.tileCutter.calcOutlines();
		this.tileCutter.tiles.forEach(tile=>{
			tile.findNeighbors(this.tileCutter.tiles,this.collide);
		});*/
		this.bg = new Background(0, 0, width, height, this.sprites.bg);
		this.player = new Player(playerX, playerY, this.sprites.player);
		this.interval = setInterval(() => {
			this.update();
			this.render();
		}, 1000 / 60);
	}
	removeEvent(eventName, target) {
		target.removeEventListener(eventName, this[eventName + 'Event']);
	}
	addEvent(eventName, target) {
		this[eventName + 'Event'] = this[eventName].bind(this);
		target.addEventListener(eventName, this[eventName + 'Event']);
	}
	initControls() {
		this.events = [
			['keyup', window],
			['keydown', window],
			['mousemove', canvas],
			['mousedown', canvas],
			['mouseup', canvas],
		];
		this.events.forEach(event => this.addEvent(...event));
	}
	mousedown(e) {
		let player = this.player;
		player.setAngle(e);
		player.aimX = e.offsetX - canvas.width / 2;
		player.aimY = e.offsetY - canvas.height / 2;
		player.shooting = true;
	}
	mouseup(e) {
		this.player.shooting = false;
	}
	mousemove(e) {
		this.player.setAngle(e);
	}
	keydown(e) {
		this.keys[e.keyCode] = true;
		if (e.keyCode === this.settings.RELOAD_KEY && !this.player.weapon.reloading) {
			this.player.weapon.reloadTime = this.time + this.player.weapon.reloadBase;
			this.player.weapon.reloading = true;
		}
	}
	keyup(e) {
		this.keys[e.keyCode] = false;
	}
	stop() {
		this.events.forEach(event => this.removeEvent(...event));
	}


	update() {
		this.time++;
		this.fadeFaders();
		this.player.shoot(this.bullets, this.time);
		if (this.player.weapon.reloading) {
			this.player.weapon.reload(this.time);
		}
		this.bullets = this.bullets.filter(bullet => {
			return bullet.move();
		});
		this.pickups = this.pickups.filter(pickup => {
			if (this.player.collides(pickup)) {
				pickup.use(this.player);
				return false;
			}
			return true;
		});
		this.hitFields = this.hitFields.filter(hitField => {
			hitField.hit(this.player);
			return hitField.active;
		});
		this.enemies.forEach(enemy => {
			enemy.checkRepath(this.time);
			if (enemy.needsPath)
				enemy.setPath(this.player, this.astarGrid, this.obstacles);
			enemy.setDirection(this.player);
			enemy.move(this.obstacles, this.player, this.width, this.height, this.time);
		});
		this.spawners.forEach(spawner => {
			spawner.spawn(this.enemies, this.time);
		});
		this.bulletCollide();
		this.handleInput();
		this.checkBounds();
	}
	initMap() {
		this.obstacles.push(new Obstacle(600, 20, 200, 200));
		this.obstacles.push(new Obstacle(100, 290, 700, 50));
		this.obstacles.push(new Obstacle(100, 500, 300, 100, Math.PI / 4));
		this.obstacles.push(new Obstacle(500, 500, 300, 100, Math.PI / 8));
		this.enemies.push(new Enemy(0, 10, 20, 20, 100, 2, 20, this.sprites.enemy));
		//this.enemies.push(new Enemy(0,10,20,20,100,2,20,this.sprites.enemy));
		this.spawners.push(new Spawner(0, 10, 600));
		this.pickups.push(new Pickup(300, 10, 20, 20));
		this.interactables.push(new Interactable(300, 110, 40, 40));
		this.astarGrid.initGrid(this.width, this.height, this.obstacles, this.collide);
	}
	fadeFaders() {
		this.faders = this.faders.filter(fader => {
			fader.fade();
			return fader.alive();
		});
	}
	bulletCollide() {
		this.bullets = this.bullets.filter(bullet => {
			this.enemies = this.enemies.filter(enemy => {
				if (bullet.collides(enemy)) {
					if (!bullet.hits.has(enemy.id) && bullet.pierce > 0) {
						enemy.hp -= bullet.damage;
						this.faders.push(new HitText(bullet.x, bullet.y, `-${bullet.damage}`));
						bullet.hits.add(enemy.id);
						bullet.pierce--;
					}
					if (enemy.hp <= 0)
						return false;
				}
				return true;
			});
			this.obstacles.forEach(obs => {
				if (obs.collides(bullet)) {
					if (!obs.allowBullets) bullet.pierce = 0;
					//bullet.setAngle(bullet.angle + Math.PI/2);
				}
			});
			if (bullet.pierce <= 0)
				return false;
			return true;
		});
	}
	checkBounds() {
		this.obstacles.forEach(obs => {
			if (obs.collides(this.player)) {
				this.backOffCollide(obs, this.player);
			}
		});
		this.bullets = this.bullets.filter(bullet => {
			return !(bullet.x + bullet.width < 0 ||
				bullet.x > this.width ||
				bullet.y + bullet.height < 0 ||
				bullet.y > this.height);
		});
		if (this.player.x < 0) this.player.x = 0;
		if (this.player.x + this.player.width > this.width) this.player.x = this.width - this.player.width;
		if (this.player.y < 0) this.player.y = 0;
		if (this.player.y + this.player.height > this.height) this.player.y = this.height - this.player.height;

	}
	/*o1 is immobile, o2 will be moved*/
	/* 
	 * Could take an approach where we do binary space partitioning between o2 and o1 based on o1's velocity vector? 
	 * and do like 5 passes and if none pass we revert o2 back by its velocity vector?
	 * Can we do this is some way where we do partial backoffs of both the x and y components of the vectors?
	 * 
	 */

	backOffCollidePlayer(o1, o2) {
		let goodX = o2.x - o2.direction.x * o2.speed;
		let goodY = o2.y - o2.direction.y * o2.speed;
		let directionX = -1;
		let directionY = -1;
		for (let i = 1; i < 6; i++) {
			const scale = 1 / (2 ** i);
			const xDiff = o2.direction.x * (o2.speed * scale) * directionX;
			const yDiff = o2.direction.y * (o2.speed * scale) * directionY;
			o2.x += xDiff;
			if (o1.collides(o2)) {
				directionX = -1;
			} else {
				directionX = 1;
				goodX = o2.x;
				console.log('goodX')
			}
			o2.x -= xDiff;
			o2.y += yDiff;
			if (o1.collides(o2)) {
				directionY = -1;
			} else {
				directionY = 1;
				goodY = o2.y;
				console.log('goodY')
			}
			o2.x += xDiff;
			/*o2.y -= yDiff;
			if(directionX === 1)
				o2.x += xDiff;
			if(directionY === 1)
				o2.y += yDiff;*/
		}
		o2.x = goodX;
		o2.y = goodY;
	}

	backOffCollide(o1, o2) {
		let goodX = o2.x - o2.direction.x * o2.speed;
		let goodY = o2.y - o2.direction.y * o2.speed;
		let direction = -1;
		for (let j = 0; j < 2; j++) {
			for (let i = 1; i < 6; i++) {
				const scale = 1 / (2 ** i);
				o2.x += o2.direction.x * (o2.speed * scale) * direction;

				if (o1.collides(o2)) {
					direction = -1;
				} else {
					direction = 1;
					goodX = o2.x;

				}
			}
			direction = -1;
			o2.x = goodX;
			for (let i = 1; i < 6; i++) {
				const scale = 1 / (2 ** i);

				o2.y += o2.direction.y * (o2.speed * scale) * direction;
				if (o1.collides(o2)) {
					direction = -1;
				} else {
					direction = 1;

					goodY = o2.y;
				}
			}

			o2.y = goodY;
		}
	}

	/*
	backOffCollide(o1, o2) {
		let pushLeft = Math.abs(o1.x - (o2.x + o2.width));
		let pushRight = Math.abs((o1.x + o1.width) - o2.x);
		let pushUp = Math.abs(o1.y - (o2.y + o2.height));
		let pushDown = Math.abs((o1.y + o1.height) - o2.y);
		let options = [pushLeft, pushRight, pushUp, pushDown];
		switch (options.indexOf(Math.min(...options))) {
			case 0:
				o2.x = o1.x - o2.width;
				break;
			case 1:
				o2.x = o1.x + o1.width;
				break;
			case 2:
				o2.y = o1.y - o2.height;
				break;
			case 3:
				o2.y = o1.y + o1.height;
				break;
		}
	}
	*/
	handleInput() {
		this.player.direction.y = 0;
		this.player.direction.x = 0;
		if (this.keys[this.settings.UP_KEY]) {
			this.player.y -= this.player.speed;
			this.player.direction.y += -1;
		}
		if (this.keys[this.settings.DOWN_KEY]) {
			this.player.y += this.player.speed;
			this.player.direction.y += 1;
		}
		if (this.keys[this.settings.LEFT_KEY]) {
			this.player.x -= this.player.speed;
			this.player.direction.x += -1;
		}
		if (this.keys[this.settings.RIGHT_KEY]) {
			this.player.x += this.player.speed;
			this.player.direction.x += 1;
		}
		if (this.keys[this.settings.INTERACT_KEY]) {
			this.doInteract();
		}

	}
	doInteract() {
		this.interactables.forEach(interactable => {
			if (this.player.collides(interactable)) {
				interactable.interact(this.time);
			}
		});
	}

	/*Creates a shifted x and y of things to be drawn based on player. Returns nothing if x and y are off map.*/
	cameraOffset(obj) {
		let adjustedX = (obj.x - (this.player.x)) + canvas.width / 2;
		let adjustedY = (obj.y - (this.player.y)) + canvas.height / 2;
		let oWidth = obj.width === undefined ? 0 : obj.width;
		let oHeight = obj.height === undefined ? 0 : obj.height;
		if (adjustedX + oWidth < 0 || adjustedX > canvas.width || adjustedY > canvas.height || adjustedY + oHeight < 0)
			return;
		return {
			x: adjustedX,
			y: adjustedY
		};
	}
	render() {
		let player = this.player;
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		ctx.fillStyle = 'black';
		ctx.fillRect(0, 0, canvas.width, canvas.height);

		/*Draw background*/
		let adjusted = this.cameraOffset(this.bg);
		if (adjusted) ctx.drawImage(this.bg.img, adjusted.x, adjusted.y, this.bg.width, this.bg.height);

		if (this.settings.AI_DEBUG) {
			for (let x = 0; x < this.astarGrid.grid.length; x++) {
				for (let y = 0; y < this.astarGrid.grid[x].length; y++) {
					const square = this.astarGrid.grid[x][y];
					const mockSquare = new RectangleEntity(square.x * this.astarGrid.unitWidth, square.y * this.astarGrid.unitHeight, square.width, square.height);
					let adjusted = this.cameraOffset(mockSquare);
					if (adjusted) {
						ctx.strokeStyle = square.wall ? 'gray' : 'white';
						ctx.strokeRect(adjusted.x, adjusted.y, mockSquare.width, mockSquare.height);
					}
				}
			}
		}



		ctx.fillStyle = 'black';
		this.bullets.forEach(bullet => {
			let adjusted = this.cameraOffset(bullet);
			if (adjusted) bullet.render(ctx, canvas, adjusted);
		});


		ctx.fillStyle = 'gray';
		this.obstacles.forEach(obstacle => {
			let adjusted = this.cameraOffset(obstacle);
			if (adjusted) obstacle.render(ctx, adjusted);
			if (this.settings.SEGMENT_DEBUG) {
				ctx.strokeStyle = 'pink';
				obstacle.lineSegments.forEach(segment => {
					let adjustedStart = this.cameraOffset(segment.start);
					let adjustedEnd = this.cameraOffset(segment.end);
					if (adjustedStart && adjustedEnd) {
						ctx.beginPath();
						ctx.moveTo(adjustedStart.x, adjustedStart.y);
						ctx.lineTo(adjustedEnd.x, adjustedEnd.y);
						ctx.stroke();
					}
				});
			}

		});

		ctx.fillStyle = 'gold';
		this.pickups.forEach(pickup => {
			let adjusted = this.cameraOffset(pickup);
			if (adjusted) pickup.render(ctx, adjusted);
		})
		ctx.fillStyle = 'purple';
		this.interactables.forEach(interactable => {
			let adjusted = this.cameraOffset(interactable);
			if (adjusted) interactable.render(ctx, adjusted);
		})

		/*Draw player*/
		this.player.render(ctx, canvas);


		ctx.strokeStyle = 'red';
		this.enemies.forEach(enemy => {
			let adjusted = this.cameraOffset(enemy);
			if (adjusted) {
				enemy.render(ctx, adjusted);
			}
			if (this.settings.AI_DEBUG) {
				ctx.beginPath();
				ctx.lineTo(canvas.width / 2 + player.width / 2, canvas.height / 2 + player.height / 2)
				enemy.path.forEach(path => {
					let adjusted = this.cameraOffset(path);
					if (adjusted) {
						ctx.lineTo(adjusted.x + path.width / 2, adjusted.y + path.height / 2);
						ctx.fillStyle = 'red';
						ctx.fillRect(adjusted.x, adjusted.y, path.width, path.height);
					}
				});
				if (adjusted) ctx.lineTo(adjusted.x + enemy.width / 2, adjusted.y + enemy.height / 2);
				ctx.stroke();

				//test line drawing for coords
				let adjustedPlayer = this.cameraOffset(player);
				let adjustedEnemy = this.cameraOffset(enemy);
				if (adjustedEnemy) {
					const getDistance = (node1, node2) => {
						return Math.sqrt(Math.pow(node2.x - node1.x, 2) + Math.pow(node2.y - node1.y, 2));
					}
					const playerDist = getDistance(player, enemy) - player.width;
					ctx.beginPath();
					ctx.strokeStyle = 'red';
					ctx.stroke();
					ctx.beginPath();
					let start = new Point(
						adjustedEnemy.x + (enemy.width / 2) + (enemy.width / 2) * Math.cos(enemy.angle + Math.PI / 2),
						adjustedEnemy.y + (enemy.height / 2) + (enemy.height / 2) * Math.sin(enemy.angle + Math.PI / 2)
					);
					let end = new Point(
						start.x + playerDist * enemy.direction.x,
						start.y + playerDist * enemy.direction.y
					);
					let intersection = this.obstacles.some(obstacle => {
						const someSegment = new LineSegment(
							new Point(
								start.x - adjustedEnemy.x + enemy.x,
								start.y - adjustedEnemy.y + enemy.y
							),
							new Point(
								end.x - adjustedEnemy.x + enemy.x,
								end.y - adjustedEnemy.y + enemy.y
							)
						)
						return obstacle.lineSegments.some(segment => {
							return someSegment.intersects(segment);
						});
					});
					if (intersection)
						ctx.strokeStyle = 'blue';
					ctx.moveTo(start.x, start.y)
					ctx.lineTo(end.x, end.y);
					ctx.stroke();

					ctx.strokeStyle = 'red';
					ctx.beginPath();
					start = new Point(
						adjustedEnemy.x + (enemy.width / 2) + (enemy.width / 2) * Math.cos(enemy.angle - Math.PI / 2),
						adjustedEnemy.y + (enemy.height / 2) + (enemy.height / 2) * Math.sin(enemy.angle - Math.PI / 2)
					);
					end = new Point(
						start.x + playerDist * enemy.direction.x,
						start.y + playerDist * enemy.direction.y
					);
					intersection = this.obstacles.some(obstacle => {
						const someSegment = new LineSegment(
							new Point(
								start.x - adjustedEnemy.x + enemy.x,
								start.y - adjustedEnemy.y + enemy.y
							),
							new Point(
								end.x - adjustedEnemy.x + enemy.x,
								end.y - adjustedEnemy.y + enemy.y
							)
						);
						return obstacle.lineSegments.some(segment => {
							return someSegment.intersects(segment);
						});
					});
					if (intersection)
						ctx.strokeStyle = 'blue';
					ctx.moveTo(start.x, start.y)
					ctx.lineTo(end.x, end.y);
					ctx.stroke();
				}
			}

		});
		ctx.fillStyle = 'green';
		if (this.settings.HIT_DEBUG) {
			ctx.globalAlpha = .2;
			this.hitFields.forEach(hitField => {
				let adjusted = this.cameraOffset(hitField);
				if (adjusted) ctx.fillRect(adjusted.x, adjusted.y, hitField.width, hitField.height);
			});
			ctx.globalAlpha = 1;
		}
		ctx.fillStyle = 'black';
		this.faders.forEach((fader) => {
			let adjusted = this.cameraOffset(fader);
			fader.render(ctx, adjusted);

		});
		ctx.globalAlpha = 1;
		if (this.settings.MINIMAP_ENABLED) this.miniMap.render(ctx, canvas, player, this.enemies, this.obstacles, this.interactables);
	}
}

class Waypoint {
	constructor(x, y) {
		this.x = x;
		this.y = y;
		this.width = 1;
		this.height = 1;
	}
}


class Background {
	constructor(x, y, width, height, img) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.img = img;
	}
}


class Spawner {
	constructor(x, y, speed) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.time = 0;
	}
	spawn(enemies, time) {
		if (this.time <= time) {
			enemies.push(new Enemy(this.x, this.y, 20, 20, 100, 2, 20, spriteManager.sprites.enemy));
			this.time = time + this.speed;
		}
	}
}

window.addEventListener('load', () => {
	game = new Game();
});