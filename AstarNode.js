class AstarNode {
	constructor(x, y, width, height, wall) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.h = 0;
		/*this.g = 0; // g is acquired score*/
		this.f = 0;
		this.wall = !!wall;
		this.neighbors = [];
	}
	clone() {
		return new AstarNode(this.x, this.y, this.width, this.height, this.wall);
	}
	reset() {
		this.f = 0;
		this.g = undefined;
		this.parent = undefined
		this.open = undefined;
		this.closed = undefined;
	}
	findNeighbors(grid) {
		let x = this.x;
		let y = this.y;
		let col = grid.length;
		let row = grid[0].length;
		if (x > 0) this.neighbors.push(grid[x - 1][y]);
		if (y > 0) this.neighbors.push(grid[x][y - 1]);
		if (x < col - 1) this.neighbors.push(grid[x + 1][y]);
		if (y < row - 1) this.neighbors.push(grid[x][y + 1]);
		if (x < col - 1 && y > 0) this.neighbors.push(grid[x + 1][y - 1]);
		if (x < col - 1 && y < row - 1) this.neighbors.push(grid[x + 1][y + 1]);
		if (x > 0 && y > 0) this.neighbors.push(grid[x - 1][y - 1]);
		if (x > 0 && y < row - 1) this.neighbors.push(grid[x - 1][y + 1]);

	}
	getArea() {
		return this.width * this.height;
	}
}