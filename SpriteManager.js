class SpriteManager{
	constructor(){
		this.sprites = {
            enemy: this.newImage('sprites/enemy.png'),
            bg: this.newImage('sprites/bg.png'),
            player: this.newImage('sprites/player.png'),
            fireball: this.newImage('sprites/fireball.png')
        }
    }
    newImage(src){
		let img = new Image();
		img.src = src;
		return img;
	}
}
