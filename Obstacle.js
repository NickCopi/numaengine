class Obstacle extends RectangleEntity{
	constructor(x, y, width, height, angle = 0) {
		super(x, y, width, height, angle);
	}
	isEnemyPassable(){
		return false;
	}
}