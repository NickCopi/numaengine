class Bullet extends RectangleEntity{
	constructor(x, y, speed, damage, pierce, radius, maxRange, sprite) {
		super(x,y,radius,radius,0);
		this.speed = speed;
		this.damage = damage;
		this.pierce = pierce;
		this.maxRange = maxRange;
		this.rangeUsed = 0;
		//should be a set not an array
		this.hits = new Set();
		this.direction = {};
		this.sprite = sprite;
	}
	getLineSegments(){
		this.calcLineSegments();
		return this.lineSegments;
	}
	setDirection(theta) {
		this.direction.x = Math.cos(theta);
		this.direction.y = Math.sin(theta);
		this.angle = theta;
	}
	move() {
		this.x += this.speed * this.direction.x;
		this.y += this.speed * this.direction.y;
		this.rangeUsed += Math.abs(this.speed * this.direction.x) + Math.abs(this.speed * this.direction.y);
		return this.rangeUsed <= this.maxRange;
	}
	render(ctx, canvas, adjusted){
		if(!this.sprite){
			ctx.fillRect(adjusted.x, adjusted.y, this.width, this.height);
			return;
		}
		ctx.save();
		ctx.translate(adjusted.x + (this.width/2), adjusted.y + (this.height/2));
		ctx.rotate(this.angle);
		ctx.drawImage(spriteManager.sprites[this.sprite], this.width / -2, this.height / -2, this.width, this.height);
		ctx.restore();
	}
}