class LineSegment {
    constructor(startPoint, endPoint) {
        this.start = startPoint;
        this.end = endPoint;
    }
    rotateAroundPoint(rotateX, rotateY, angle) {
        const cosAngle = Math.cos(angle);
        const sinAngle = Math.sin(angle);
        const points = [this.start, this.end];
        points.forEach(point => {
            const x = point.x - rotateX;
            const y = point.y - rotateY;
            point.x = rotateX + x * cosAngle - y * sinAngle;
            point.y = rotateY + x * sinAngle + y * cosAngle;
        })
    }

    /*
    ccw(p1, p2, p3){
        return (p3.y - p1.y) * (p2.x - p1.x) > (p2.y - p1.y) * (p3.x - p1.x);
    }
	
    intersects(segment){
        const p1 = this.start;
        const p2 = this.end;
        const p3 = segment.start;
        const p4 = segment.end;
        return this.ccw(p1, p3, p4) !== this.ccw(p2, p3, p4) && this.ccw(p1, p2, p3) !== this.ccw(p1, p2, p4);
    }*/

    orientation(p, q, r) {
        const val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
        if (val === 0) return 0;  // Collinear
        return (val > 0) ? 1 : 2; // Clockwise or Counterclockwise
    }
    
    onSegment(p, q, r) {
        return (
            q.x <= Math.max(p.x, r.x) &&
            q.x >= Math.min(p.x, r.x) &&
            q.y <= Math.max(p.y, r.y) &&
            q.y >= Math.min(p.y, r.y)
        );
    }
    
    intersects(otherSegment) {
        const p1 = this.start;
        const q1 = this.end;
        const p2 = otherSegment.start;
        const q2 = otherSegment.end;
    
        const o1 = this.orientation(p1, q1, p2);
        const o2 = this.orientation(p1, q1, q2);
        const o3 = this.orientation(p2, q2, p1);
        const o4 = this.orientation(p2, q2, q1);
    
        if (o1 !== o2 && o3 !== o4) return true;
    
        if (o1 === 0 && this.onSegment(p1, p2, q1)) return true;
        if (o2 === 0 && this.onSegment(p1, q2, q1)) return true;
        if (o3 === 0 && this.onSegment(p2, p1, q2)) return true;
        if (o4 === 0 && this.onSegment(p2, q1, q2)) return true;
    
        return false;
    }
}