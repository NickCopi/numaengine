class MiniMap {
    constructor(worldWidth, worldHeight) {
        this.width = 100;
        this.height = 100;
        this.scaleX = this.width / worldWidth;
        this.scaleY = this.height / worldHeight;
    }
    scaledRect(ctx, rect, offsetX, offsetY) {
        ctx.fillRect(offsetX + (rect.x * this.scaleX), offsetY + (rect.y * this.scaleY), rect.width * this.scaleX, rect.height * this.scaleY);
    }
    render(ctx, canvas, player, enemies, obstacles, interactables) {
        const offsetX = canvas.width - this.width;
        const offsetY = canvas.height - this.height;
        ctx.globalAlpha = 0.2;
        ctx.fillStyle = 'darkgreen';
        ctx.fillRect(offsetX, offsetY, this.width, this.height)
        ctx.globalAlpha = 1;
        ctx.fillStyle = 'gray';
        obstacles.forEach(obstacle => {
            this.scaledRect(ctx, obstacle, offsetX, offsetY);
        });
        ctx.fillStyle = 'gray';
        obstacles.forEach(obstacle => {
            this.scaledRect(ctx, obstacle, offsetX, offsetY);
        });
        ctx.fillStyle = 'purple';
        interactables.forEach(interactable => {
            this.scaledRect(ctx, interactable, offsetX, offsetY);
        });
        ctx.fillStyle = 'red';
        enemies.forEach(enemy => {
            this.scaledRect(ctx, enemy, offsetX, offsetY);
        });
        ctx.fillStyle = 'green';
        this.scaledRect(ctx, player, offsetX, offsetY);

    }
}
